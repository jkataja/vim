au BufNewFile,BufRead *.F90 set filetype=fortran
au BufNewFile,BufRead *.f90 set filetype=fortran

augroup filetypedetect
au BufNewFile,BufRead *.sif setf sif
augroup END
